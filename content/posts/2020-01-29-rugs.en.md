---
title: RUGS 2020
description: "Our plan for R Consortium’s R User Group and Small Conference Support Program for the 2020"
date: 2020-01-29T09:00:00+03:00
publishDate: 2020-01-29T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

The [R Consortium](https://www.r-consortium.org/)
offers financial support to R User Groups around the world
with the goal of promote face-to-face meetings.

For groups that are starting,
the R Consortium offers "New Group" financial support
that includes US$100.

<!--more-->

## Activities

In 2020,
we are planning the following activities:

- Weekly meetings related with [TidyTuesday](https://github.com/rfordatascience/tidytuesday)
- Monthly meetings where a member of the group will give a talk related with R
- One Software Carpentry workshop in August or September for 20 learners

## Budget

| Activity                       | Unit Cost (BRL) | Units      | Cost (BRL) |
| ------------------------------ | ---------------:| ----------:| ----------:|
| Monthly meeting                |              12 |         10 |        120 |
| Software Carpentry workshop    |             300 |          1 |        300 |