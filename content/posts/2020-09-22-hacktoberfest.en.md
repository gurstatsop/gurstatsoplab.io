---
title: Take part in Hacktoberfest
description: "Earn a limited T-Shirt this October"
date: 2020-09-22T10:00:00+03:00
publishDate: 2020-09-22T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["Hacktoberfest", "DigitalOcean", "GitHub", "Open Source"]
---

[Hacktoberfest](https://hacktoberfest.digitalocean.com/) encourages participation in the open source community
as a global community challenge
where participants can earn a limited edition T-shirt.

<!--more-->

## Participation rules

From https://hacktoberfest.digitalocean.com/details:

> To get a shirt, you must make four pull requests (PRs) between October 1–31 in any time zone. Pull requests can be to any public repository on GitHub, not just the ones highlighted. The pull request must contain commits you made yourself. If a maintainer reports your pull request as spam, it will not be counted toward your participation in Hacktoberfest. If a maintainer reports behavior that’s not in line with the project’s code of conduct, you will be ineligible to participate. This year, the first 75,000 participants can earn a T-shirt.

And don't forget to join [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).