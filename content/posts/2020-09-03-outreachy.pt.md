---
title: Inscrições no Estágio do Outreachy estão abertas
description: "Inscreva-se até o dia 20 de Setembro de 2020"
date: 2020-09-02T10:00:00+03:00
publishDate: 2020-09-02T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["Outreachy"]
---

[Outreachy](https://www.outreachy.org) prover estágio de três meses
para trabalhar em um projeto de software livre e aberto (FOSS),
como o R.
Projetos de estagiários no Outreachy podem incluir programação,
experiência de usuário,
documentação,
ilustração,
e
designer gráfico,
ou ciência de dados.
Normalmente,
estagiários encontram emprego depois do estágio com patrocinadores do Outreachy
ou em posições que empregam as competências desenvolvidas durante o estágio.

<!--more-->

O próximo estágio será entre Dezembro e Março.
O salário **total** dos estagiários é de US$5.500 pelos três meses de trabalho.
Estagiários também possuem acesso à US$500 para participação em eventos.

Visite https://www.outreachy.org/apply/ hoje e inscreva-se!

E não esqueça de participar do [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).