---
title: Bolsas PCI no IBICT
description: "Oportunidades em Brasília com R"
date: 2020-06-18T10:00:00+03:00
publishDate: 2020-06-18T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

O [Instituto Brasileiro de Informação em Ciência e Tecnologia](http://www.ibict.br/) (IBICT)
tornou público o [Edital nº 01/2020](http://www.ibict.br/editais4/programa-de-capacitacao-institucional-pci/item/2212-1-processo-seletivo-periodo-de-inscricao-17-06-2020-a-30-06-2020)
referente ao processo seletivo de bolsistas (no Subprograma de Capacitação Institucional)
para o desenvolvimento dos seguinte projetos:

- Realização de estudos que orientem o mapeamento, prospecção, análise e uso de inovação em bibliografia e documentação.
- Realização de prospecção, estudos para a atuação e criação de infraestrutura de ciência aberta no Brasil.
- Realização de estudos que visem traçar novas estratégias, metodologias e técnicas para garantir a qualidade e periodicidade de publicações técnicocientífica.
- Pesquisa e consolidação de aspectos relacionados à visualização e ciência de dados com focos nos dados públicos oficiais.
- Divulgação científica, popularização da ciência e inclusão social.

<!--more-->

As bolsas são no valor de R$2.860,00
e os bolsistas devem desenvolver suas atividades em Brasília.
Inscrições encerram-se no dia 30 de Junho de 2020.

Não esqueça de também inscreva-se no [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).