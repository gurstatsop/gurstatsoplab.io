---
title: Vídeos da useR! 2020 disponíveis
description: "Sessão de contribuidores estão no YouTube"
date: 2020-07-08T10:00:00+03:00
publishDate: 2020-07-08T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["userR"]
---

[useR!](https://user2020.r-project.org/)
é a conferência anual dedicada ao R.
[Sessão de contribuidores](https://user2020.r-project.org/program/contributed/)
estão no [YouTube](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg/playlists).

<!--more-->

E não esqueça de participar do [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).