---
title: "Member: Raniere Silva"
description: "Find out about the group founder"
date: 2020-01-28T09:00:00+03:00
publishDate: 2020-01-28T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

Raniere worked at the University of Manchester, United Kingdom
where contributed to the [Software Sustainability Institute](https://software.ac.uk).
Between 2017 and 2018,
Raniere participated in the [Manchester R User Group](https://www.staffnet.manchester.ac.uk/news/display/?id=17377) and [Sheffield R User Group](https://www.staffnet.manchester.ac.uk/news/display/?id=17377).
Back to Brazil,
Raniere seek share his experience in the United Kingdom with other R users.