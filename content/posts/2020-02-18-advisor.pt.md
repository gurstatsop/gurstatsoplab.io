---
title: Nossos conselheiros em 2020
description: "Descubra que estará ajudando de longe"
date: 2020-02-18T09:00:00+03:00
publishDate: 2020-02-18T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

Alejandra Gonzalez-Beltran,
Fernando Mayer,
Beatriz Milz
e
Renato A. Corrêa dos Santos
são os conselheiros do grupo em 2020.

<!--more-->

<div class="container">
    <div class="row">
    <div class="col-3">
        <img src="/photo/advisor/alejandra-gonzalez-beltran.jpg"><strong>Alejandra Gonzalez-Beltran</strong><br>Science and Technology Facilities Council, UK
    </div>
    <div class="col-3">
        <img src="/photo/advisor/fernando-mayer.jpg"><strong>Fernando Mayer</strong><br>LEG/DEST/UFPR
    </div>
    <div class="col-3">
        <img src="/photo/advisor/beatriz-milz.jpg"><strong>Beatriz Milz</strong><br>Instituto de Energia e Ambiente/Universidade de São Paulo (IEE/USP)
    </div>
        <div class="col-3">
        <img src="/photo/advisor/renato-santos.jpg"><strong>Renato A. Corrêa dos Santos</strong><br>UNICAMP
    </div>
    </div>
</div>

Cada conselheiro irá voluntariar 30 minutos por mês
para conversar com os líderes do grupo
e contribuir na escolha dos temas para os encontros.

Inscreva-se no [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
para receber notificação dos encontros que começarão em Março.