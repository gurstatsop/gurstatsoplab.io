---
title: LatinR2020 Call for Paper
description: "Deadline is April 30, 2020"
date: 2020-03-06T09:00:00+03:00
publishDate: 2020-03-06T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

We added to [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
our first events..

<!--more-->

## Tidy Tuesday

This is a practical session
where you will improve your data processing and visualisation skills.
We will use data from [https://github.com/rfordatascience/tidytuesday](https://github.com/rfordatascience/tidytuesday).

This is the perfect opportunity for you to build your GitHub/GitLab/Twitter profile.

## R Clinic

In this session we will help you to resolve problems that you are having with R.

## What to Bring

For both sessions,
bring your laptop with charger
and
your favourite R cheatsheet.

## Date and Place

Tuesdays and Thursdays from 12:30 until 13:30
at Antiga Sala do GOAL
locate at ICEB 1, Bloco D, Pavimento Inferior
da Universidade Federal de Ouro Preto, Campus Universitário Morro do Cruzeiro, Ouro Preto.

Join [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
to receive notification of this and future meetings.