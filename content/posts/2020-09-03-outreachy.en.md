---
title: Outreachy Internship applications are open
description: "Apply before 20 September 2020"
date: 2020-09-02T10:00:00+03:00
publishDate: 2020-09-02T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["Outreachy"]
---

[Outreachy](https://www.outreachy.org) provides three-month internships
to work in Free and Open Source Software (FOSS),
like R.
Outreachy internship projects may include programming,
user experience,
documentation,
illustration
and
graphical design,
or data science.
Interns often find employment after their internship with Outreachy sponsors
or in jobs that use the skills they learned during their internship.

<!--more-->

The next internships will run from December to March.
Interns are paid a stipend of $5,500 USD for the three months of work.
They also have a $500 USD stipend to travel to conferences and events.

Visit https://www.outreachy.org/apply/ today to apply.

And don't forget to join [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).