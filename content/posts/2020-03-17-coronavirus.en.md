---
title: Meetings cancelled
description: "As step to coronavirus prevention"
date: 2020-03-17T20:00:00+03:00
publishDate: 2020-03-17T20:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

The Federal University of Ouro Preto (UFOP)
[announced](https://ufop.br/noticias/institucional/suspensas-atividades-presenciais-na-ufop)
that their activities will be interrupted
as prevention to coronavirus.

<!--more-->

Our meetings,
Tidy Tuesday
and
R Clinic,
are cancelled until UFOP return their activities to normal.

Join [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
to receive notification when our meetings re-start.