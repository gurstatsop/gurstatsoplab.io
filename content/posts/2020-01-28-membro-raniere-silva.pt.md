---
title: "Membro: Raniere Silva"
description: "Descubra sobre o fundador do grupo"
date: 2020-01-28T09:00:00+03:00
publishDate: 2020-01-28T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

Raniere trabalhou na Universidade de Manchester no Reino Unido
onde contribuiu para o [Software Sustainability Institute](https://software.ac.uk).
Entre os anos de 2017 e 2018,
Raniere participou dos groupos de R em [Manchester](https://www.staffnet.manchester.ac.uk/news/display/?id=17377) e [Sheffield](https://www.staffnet.manchester.ac.uk/news/display/?id=17377).
De volta ao Brasil,
Raniere busca compartilhar a experiência que teve no Reino Unido
com outros usuários de R.