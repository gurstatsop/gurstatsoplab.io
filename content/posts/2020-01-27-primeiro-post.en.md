---
title: First Post
description: "Introducing the R User Group"
date: 2020-01-27T09:00:00+03:00
publishDate: 2020-01-27T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

Is a pleasure to invite you
to participate in the R User Group.
The activities will start in March
and, soon, we will provide a calendar
with more information.