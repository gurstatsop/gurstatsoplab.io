---
title: Estamos Buscando Novo Coordenador
description: "Raniere está indo para Hong Kong"
date: 2020-09-29T10:00:00+03:00
publishDate: 2020-09-29T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["Liderança"]
---

Raniere Silva está indo para Hong Kong
onde estará continuado seus estudos
na direção de conquistar seu doutorado
e
está deixando
a coordenação do Grupo de Usuários de R em Ouro Preto.

<!--more-->

Estamos procurando um novo coordenador
para continuar prmovendo R em nossa comunidade.
Entre em contato através do email
[gurstatsop@gmail.com](mailto:gurstatsop@gmail.com)
para contribuir com o grupo.

E não esqueça de participar do [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).