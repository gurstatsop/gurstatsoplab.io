---
title: useR! 2020 Videos available
description: "Contributed sessions are on YouTube"
date: 2020-07-08T10:00:00+03:00
publishDate: 2020-07-08T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["userR"]
---

[useR!](https://user2020.r-project.org/)
is the yearly conference dedicated to R.
[Contributed sessions](https://user2020.r-project.org/program/contributed/)
are available on [YouTube](https://www.youtube.com/channel/UC_R5smHVXRYGhZYDJsnXTwg/playlists).

<!--more-->

And don't forget to join [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).