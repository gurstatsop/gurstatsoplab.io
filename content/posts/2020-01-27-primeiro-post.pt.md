---
title: Primeiro Post
description: "Apresentando o Grupo de R"
date: 2020-01-27T09:00:00+03:00
publishDate: 2020-01-27T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

É com grande prazer que convido todos
para participar do grupo de R.
As atividades do grupo terão início em Março
e um calendário com maiores informações será divulgado aqui
em breve.