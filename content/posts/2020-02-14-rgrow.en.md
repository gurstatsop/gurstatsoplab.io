---
title: We are growing
description: "We are close to 10 members"
date: 2020-02-14T09:00:00+03:00
publishDate: 2020-02-14T09:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

We are less than one month old
and [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
is near to reach the 10 members goal.

<!--more-->

![Grow of our group](/R/members/2020-02-14.jpeg)

Join [our Meetup group](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
to receive notification of our meetings that will start in March.