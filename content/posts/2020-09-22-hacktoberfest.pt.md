---
title: Participe do Hacktoberfest
description: "Ganhe um camisa exclusiva nesse mês de Outubro"
date: 2020-09-22T10:00:00+03:00
publishDate: 2020-09-22T10:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: ["Hacktoberfest", "DigitalOcean", "GitHub", "Open Source"]
---

[Hacktoberfest](https://hacktoberfest.digitalocean.com/) encoraja participação em comunidades desenvolvedoras de código aberto
no formato de um desafio global
no qual participantes pode ganhar uma camisa de edição limitada.

<!--more-->

## Regras para Participação

Em tradução literal de
https://hacktoberfest.digitalocean.com/details:

> Para ganhar a camisa,
> você deve fazer quatro pull requests (PRs) entre 1 e 31 de outubro em qualquer lugar da terra.
> Pull requests podem ser feitos parra qualqer repositório público no GitHub,
> não apenas nos repositórios destacados.
> O pull request precisa conter contribuições feitas por você.
> Se um mantenedor reportar seu pull request como spam,
> o pull request não irá contar como contribuição para o Hacktoberfest.
> Se um mantenedor reportar comportamento em desacordo com o código de conduta do projeto,
> sua participação será cancelada.
> Nesse ano,
> os primeiro 75.000 participantes podem ganhar a camisa exclusiva.

E não esqueça de participar do [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/).