---
title: Suspensas as atividades presenciais 
description: "Para prevenção e contenção do contágio do Coronavírus"
date: 2020-03-17T20:00:00+03:00
publishDate: 2020-03-17T20:00:00+03:00
author: "Raniere Silva"
images: []
draft: false
tags: []
---

A Universidade Federal de Ouro Preto (UFOP)
publicou [nota](https://ufop.br/noticias/institucional/suspensas-atividades-presenciais-na-ufop)
anunciando o interrompimento das atividades
em decorrência do coronavírus.

<!--more-->

Nossos eventos,
Tidy Tuesday
e
Monitoria de R,
ficarão interrompidos até as atividades na UFOP serem re-iniciadas.

Inscreva-se no [nosso grupo no Meetup](https://www.meetup.com/Grupo-de-Usuarios-de-R-em-Ouro-Preto/)
para receber notificação sobre o re-início dos encontros.