---
title: "About"
description: ""
images: []
draft: false
menu: main
weight: 4
type: "page"
---

Ouro Preto R User Group
was created in 2020
with the goal of promote the use of the R language at
the Federal University of Ouro Preto
and Minas Gerais Federal Institute Campus Ouro Preto.

## Advisors

<div class="container">
    <div class="row">
    <div class="col-3">
        <img src="/photo/advisor/alejandra-gonzalez-beltran.jpg"><strong>Alejandra Gonzalez-Beltran</strong><br>Science and Technology Facilities Council, UK
    </div>
    <div class="col-3">
        <img src="/photo/advisor/fernando-mayer.jpg"><strong>Fernando Mayer</strong><br>LEG/DEST/UFPR
    </div>
    <div class="col-3">
        <img src="/photo/advisor/beatriz-milz.jpg"><strong>Beatriz Milz</strong><br>Instituto de Energia e Ambiente/Universidade de São Paulo (IEE/USP)
    </div>
        <div class="col-3">
        <img src="/photo/advisor/renato-santos.jpg"><strong>Renato A. Corrêa dos Santos</strong><br>UNICAMP
    </div>
    </div>
</div>

## Sponsors

We are grateful to our sponsors
for their financial support to our activities.

![R Consortium](/logo/sponsor/rconsortium.png)

## Code of Conduct

All individuals participating in activities
organised by Ouro Preto R User Group
must be prepared to follow
the [R Community Code of Conduct](https://wiki.r-consortium.org/view/R_Consortium_and_the_R_Community_Code_of_Conduct).