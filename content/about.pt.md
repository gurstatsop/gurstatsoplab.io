---
title: "Sobre"
description: ""
images: []
draft: false
menu: main
weight: 4
type: "page"
---

O Grupo de Usuários de R em Ouro Preto
foi fundado em 2020
com o objetivo de promover o uso da linguagem R
na Universidade Federal de Ouro Preto
e no Instituto Federal de Minas Gerais - Campus Ouro Preto.

## Conselheiros

<div class="container">
    <div class="row">
    <div class="col-3">
        <img src="/photo/advisor/alejandra-gonzalez-beltran.jpg"><strong>Alejandra Gonzalez-Beltran</strong><br>Science and Technology Facilities Council, UK
    </div>
    <div class="col-3">
        <img src="/photo/advisor/fernando-mayer.jpg"><strong>Fernando Mayer</strong><br>LEG/DEST/UFPR
    </div>
    <div class="col-3">
        <img src="/photo/advisor/beatriz-milz.jpg"><strong>Beatriz Milz</strong><br>Instituto de Energia e Ambiente/Universidade de São Paulo (IEE/USP)
    </div>
        <div class="col-3">
        <img src="/photo/advisor/renato-santos.jpg"><strong>Renato A. Corrêa dos Santos</strong><br>UNICAMP
    </div>
    </div>
</div>
  
## Patrocinadores

Somos gratos aos patrocinadores que temos
pelo suporte financeiro às nossas atividades.

![R Consortium](/logo/sponsor/rconsortium.png)

## Código de Conduta

Todos os indivíduos participando em atividades
organisadas pelo Grupo de Usuários de R em Ouro Preto
devem seguir
o [Código de Conduta da Comunidade R](https://wiki.r-consortium.org/view/R_Consortium_and_the_R_Community_Code_of_Conduct).