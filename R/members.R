library(tidyverse)

data <- read.delim(
  "data/2020-02-14.csv",
  stringsAsFactors = FALSE
)

data[, "Joined.Group.on"] <- as.factor(
    as.Date(
    data[, "Joined.Group.on"],
    format = '%B %d, %Y'
  )
)

member.grow <- data %>%
  group_by(Joined.Group.on) %>%
  tally() %>%
  mutate(cumsum.members = cumsum(n)) %>%
  mutate_if(is.factor, as.character)

# Plot from https://stackoverflow.com/a/15844938
ggplot(
  member.grow,
  aes(Joined.Group.on, cumsum.members)
) + geom_bar(stat = "identity") +
  xlab("Date") +
  ylab("Number of Members")

png(
  "static/R/members/2020-02-14.png",
  600,
  400
)
