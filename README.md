# Grupo de Usuários de R em Ouro Preto - Site

Esse é o código fonte do site.

## Static Site Generator

Utilizamos [Hugo](https://gohugo.io/) para a construir o site.

## Tema

Utilizamos [Terrassa](https://gitlab.com/gurstatsop/hugo-terrassa-theme) como tema.

## Layout

Utilizamos [Simple Grid](https://simplegrid.io/) para a construir layout de columas.

## Como adicionar conteúdo

Adicione arquivos em [content/posts](content/posts).
Olhe arquivos existentes como exemplo.